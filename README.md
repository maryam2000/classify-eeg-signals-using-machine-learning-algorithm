# Classify EEG Signals using Machine Learning Algorithm

This is the Signals and Systems Course Project
- **Aim:** Train a model for the word recognition experiment in MATLAB.
- This project includes two phases:
- Pre-processing EEG data including filtering, down sampling, epoching, and removing group delay.
- Clustering electrodes based on correlation.
- Feature extraction, Feature selection to tain the model.
